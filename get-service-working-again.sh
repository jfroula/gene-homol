#!/bin/sh
# The puropse of this script is to get the last service up and
# running after a restart because we need to warm the cache.
#
# The first curl call will launch lastal and should fail due to
# timeout error?...I think.  When this finishes (i.e. wait 5 minutes), another
# job will be submitted and is expected to succeed.

S=/global/dna/kbase/reference/img/seq

# this one should work? Maybe do again
if [ $(ps aux|grep lastal|grep -v grep|wc -l) -eq 0 ] ; then
  for try in {1..2}; do
    echo "try $try"
    echo "curl -s -X POST -T $S http://genehomology.kbase.us/img/namespace/imgnr/search > /tmp/jefftest.txt "
    curl -s -X POST -T $S http://genehomology.kbase.us/img/namespace/imgnr/search > /tmp/jefftest.txt
    echo "waiting 5 minutes for last to complete..."
    sleep $((5*60))
  done
fi

cat /tmp/jefftest.txt
