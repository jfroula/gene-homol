#!/usr/bin/env python
# python=3
# This script tests that the gene homology service is up and running.  
# It runs a curl command and returns json output that is tested for some key values.

import sys
import json
import subprocess
import smtplib

#
# lets run the curl command with a test query sequence. This command should return a short json output 
#
cmd = 'S=/global/dna/kbase/reference/img/seq && curl -s -X POST -T $S http://genehomology.kbase.us/img/namespace/imgnr/search | jq \'.| {impl: .namespaces[].impl, db: .namespaces[].database, id: .namespaces[].id, seq_len: .alignments[0].targetlenseq}\''

print(cmd)
process=subprocess.run(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True, check=True)
output = process.stdout
stderror = process.stderr
thereturncode = process.returncode

if (thereturncode != 0 or not output):
    print("Error: %s" % stderror)
    sys.exit()

#
# Convert json to dict and check we have expected output
# output should be something like this
# output='{ "impl": "last", "db": "CI Refdata", "id": "imgnr", "seq_len": 418 }'
#
data = json.loads(output)

message=''
if (data["impl"] == 'last'):
    print("impl=last....OK")
else:
    message = "implementation should be last. Found: {}".format(data["impl"])
    print(message)

if (data["db"] == 'CI Refdata'):
    print("db=CI Refdata....OK")
else:
    message="database should be \'CI Refdata\'. Found: {}".format(data["db"])
    print(message)

if (data["id"] == 'imgnr'):
    print("id=imgnr....OK")
else:
    message="database id should be \'imgnr\'. Found: {}".format(data["id"])
    print(message)

if (data["seq_len"] > 10):
    print("seq_len >10bp [%sbp]....OK" % data["seq_len"])
else:
    message="target sequence length should be greater than 10'. Found: {}".format(data["seq_len"])
    print(message)

if not message:
    print("gene-homology service is alive and well")
