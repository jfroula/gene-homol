#!/bin/bash 
set -e
#. /opt/modules/default/etc/modules.sh
#module load python3 #/3.6-anaconda-4.4

export PYTHON_DIR=/global/common/cori/software/python/3.6-anaconda-5.2
export PYTHONSTARTUP=''
export PYTHONUSERBASE=/global/homes/j/jfroula/.local/cori/3.6-anaconda-5.2
export PATH=/global/common/cori/software/python/3.6-anaconda-5.2/lib/python3.6/site-packages/mpi4py/bin:/global/common/cori/software/python/3.6-anaconda-5.2/bin:$PATH

scripts=$(realpath $0)
scripts=$(dirname $scripts)
$scripts/gene_homology_status.py
