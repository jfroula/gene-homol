# Summary

## re-start
Here are the scripts required to test and re-start the gene homology service. When there is a new Db from Torben, we need to re-start things by running `get-service-working-again.sh` which will "warm the cache".

## monitoring health
A cronjob (every night) will run `gene_homol_wrapper.sh` which runs `gene_homology_status.py` and checks that we get the expected results.

## usefull link to googledocs
https://docs.google.com/document/d/1pE0qLl0V7PtR0V8nKRKebJOX6_sfEKpDofNC2YzwLnE/edit#heading=h.mqt8p685q1hn

### Important Paths
Everything can be found under…

/global/dna/kbase/reference/

The Spin related stuff can be found in:

/global/dna/kbase/reference/spin/genehomology-kbase-us

The reference data can be found in:

/global/dna/kbase/reference/img
